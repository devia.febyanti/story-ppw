from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
import requests
import json
from django.contrib.auth.models import User, auth
from django.contrib import messages

def home(request):
    return render(request, 'main/home.html')

def daftar_buku(request):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + request.GET['q']
    hasil = requests.get(url)
    data = json.loads(hasil.content)
    return JsonResponse(data, safe = False)

def cari_buku(request):
    try:
        request.session['has_search'] = request.session['has_search'] + 1
    except:
        request.session['has_search'] = 1
    response = {
        "count":request.session['has_search']
    }
    return render(request, 'main/daftar_buku.html',response)

def register(request):
    try:
        if request.method == "POST":
            first_name = request.POST['first_name']
            last_name = request.POST['last_name']
            username = request.POST['username']
            email = request.POST['email']
            password1 = request.POST['password1']
            password2 = request.POST['password2']
            if password1 == password2:
                if User.objects.filter(username=username).exists():
                    messages.info(request, 'Username sudah digunakan')
                    return redirect('main:register')
                elif User.objects.filter(email=email).exists():
                    messages.info(request, 'Email sudah terdaftar')
                    return redirect('main:register')
                else:
                    user = User.objects.create_user(username=username, password=password1, email=email, first_name=first_name, last_name=last_name)
                    user.save()
                    return redirect('main:login')
            else:
                messages.info(request, 'Password tidak sesuai')
                return redirect('main:register')
            return redirect('main:home')
        else:
            return render(request, 'main/register.html')
    except:
        messages.info(request, 'Lengkapi form di bawah!')
        return redirect('main:register')
    
def login(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('main:home')
        else:
            messages.info(request, 'Username atau password tidak terdaftar. Silakan registrasi terlebih dahulu')
            return redirect('main:login')
    else:
        return render(request, 'main/login.html')

def logout(request):
    auth.logout(request)
    return redirect('main:home')