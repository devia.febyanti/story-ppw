from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.apps import apps
from . import views
import json
from django.contrib.auth.models import User, auth
from django.contrib.auth import authenticate, login


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)


class MainFunctionalTestCase(FunctionalTestCase):
    def test_root_url_exists(self):
        self.selenium.get(f'{self.live_server_url}/')
        html = self.selenium.find_element_by_tag_name('html')
        self.assertNotIn('not found', html.text.lower())
        self.assertNotIn('error', html.text.lower())


class Story7(TestCase):
    def test_url_kosong_ada(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/home.html')
    
    def test_url_dan_template_home_ada(self):
        response = self.client.get('/home')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/home.html')

    def test_text_pada_halaman_home_ada(self):
        response = self.client.get('/home')
        kembalian = response.content.decode('utf8')
        self.assertIn("Aktivitas Saat Ini", kembalian)
        self.assertIn("Pengalaman", kembalian)
        self.assertIn("Prestasi", kembalian)
        self.assertIn("Hobi", kembalian)


class Story8(TestCase):
    def test_url_bukuada(self):
        response = self.client.get('/url-buku')
        self.assertEqual(response.status_code, 200)
    
    def test_template_url_buku_ada(self):
        response = self.client.get('/url-buku')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response,'main/daftar_buku.html')
    
    def test_url_cari_buku_ada(self):
        response = self.client.get('/daftar-buku?q=fisika')
        self.assertEqual(response.status_code, 200)

    def test_text_tabel_ada(self):
        response = self.client.get('/url-buku')
        kembalian = response.content.decode('utf8')
        self.assertIn("Buku apa yang ingin kamu cari?", kembalian)
        self.assertIn("No.", kembalian)
        self.assertIn("Cover Buku", kembalian)
        self.assertIn("Judul", kembalian)
        self.assertIn("Jumlah Halaman", kembalian)
        self.assertIn("Penulis", kembalian)
        self.assertIn("Penerbit", kembalian)
        self.assertIn("Tanggal Publikasi", kembalian)
        self.assertIn("Link Download", kembalian)

    def test_views_buku_ada(self):
        self.buku = reverse("main:url-buku")
        response = self.client.get(self.buku)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'main/daftar_buku.html')

    def test_get_buku(self):
        kembalian = resolve(reverse('main:daftar-buku'))
        self.assertTrue(kembalian.func, views.daftar_buku)


class Story9(TestCase):
    def test_url_register_ada(self):
        response = self.client.get('/register')
        self.assertEqual(response.status_code, 200)
    
    def test_template_register_ada(self):
        response = self.client.get('/register')
        self.assertTemplateUsed(response,'main/register.html')
    
    def test_text_halaman_register_ada(self):
        response = self.client.get('/register')
        kembalian = response.content.decode('utf8')
        self.assertIn("Nama Depan", kembalian)
        self.assertIn("Nama Belakang", kembalian)
        self.assertIn("Username", kembalian)
        self.assertIn("Email", kembalian)
        self.assertIn("Password", kembalian)
        self.assertIn("Konfirmasi Password", kembalian)
    
    def test_url_login_ada(self):
        response = self.client.get('/login')
        self.assertEqual(response.status_code, 200)
    
    def test_template_login_ada(self):
        response = self.client.get('/login')
        self.assertTemplateUsed(response,'main/login.html')
    
    def test_text_halaman_login_ada(self):
        response = self.client.get('/login')
        kembalian = response.content.decode('utf8')
        self.assertIn("Username", kembalian)
        self.assertIn("Password", kembalian)
        self.assertIn("Belum memiliki akun?", kembalian)
    
    def test_register_user_berhasil(self):
        user = User.objects.create_user(username='deviafbynt', password='devia123', email='devia@gmail.com', first_name='Devia', last_name='Febyanti')
        user.save()
        self.assertEqual(User.objects.all().count(), 1)

    def test_login_user_berhasil(self):
        user = User.objects.create(username='rioark')
        user.set_password('123')
        user.save()
        logged_in = Client().login(username='rioark', password='123')
        self.assertTrue(logged_in)
    
    def test_login_user_gagal_password(self):
        user = User.objects.create(username='rioark')
        user.set_password('123')
        user.save()
        logged_in = Client().login(username='rioark', password='12345')
        self.assertFalse(logged_in)

    def test_login_user_gagal_username(self):
        user = User.objects.create(username='rioark')
        user.set_password('123')
        user.save()
        logged_in = Client().login(username='rio', password='123')
        self.assertFalse(logged_in)
    
    def test_logout(self):
        self.client.logout()