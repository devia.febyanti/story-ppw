from django.urls import path
from .views import *

app_name = 'main'

urlpatterns = [
    path('', home, name='home'),
    path('home', home, name='home'),
    path('url-buku', cari_buku, name='url-buku'),
    path('daftar-buku', daftar_buku, name='daftar-buku'),
    path('register', register, name='register'),
    path('login', login, name='login'),
    path('logout', logout, name='logout'),
]